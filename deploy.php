<?php
namespace Deployer;

require 'recipe/symfony3.php';

inventory('app/config/hosts.yml');

// Configuration

set('repository', 'git@bitbucket.org:metalmini/mijnsubclub.git');
set('git_tty', true);
set('shared_dirs', [
        'var',
    ]);
add('writable_dirs', []);

set('default_stage', 'test');



// Tasks

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

//before('deploy:symlink', 'database:migrate');
