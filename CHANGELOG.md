Changelog
=========


(unreleased)
------------
- Bugfixes. [Michael Schouman]
- Changelog update. [Michael Schouman]
- Bugfixes en testresultaten verwerkt. [Michael Schouman]
- Quickfix op editten van verbruik. [Michael Schouman]
- Changelog update. [Michael Schouman]
- Merged VerbruikIngave into develop. [michael schouman]
- Changelog update. [Michael Schouman]
- Merged lidmaatschapsjaar into develop. [michael schouman]
- Lidmaatschapsjaar toegevoegd + ORM behavior. [Michael Schouman]
- Dashboard zodra je ingelogd bent en nette foutmelding bij verkeerde
  logingegevens. [Michael Schouman]
- Merged changelog into develop. [michael schouman]
- Klikbare changelog onder versienummer. [Michael Schouman]
- Merged services into develop. [michael schouman]


1.0.0 (2017-09-28)
------------------
- Verbruik kunnen registreren plus nog wat tweaks. [Michael Schouman]
- Homestead integration. [Michael Schouman]
- Some fixes and updates. [Michael Schouman]
- Some fixes and updates. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff and stuff. [Michael Schouman]
- Stuff. [Michael Schouman]
- Initial commit. [Michael Schouman]


