<?php

namespace MijnsubclubBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MijnsubclubBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
