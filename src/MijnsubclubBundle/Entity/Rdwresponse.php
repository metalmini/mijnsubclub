<?php

namespace MijnsubclubBundle\Entity;

use Symfony\Component\Serializer\Annotation;
use DateTime;


class Rdwresponse
{
    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $aantal_cilinders;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $aantal_deuren;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $aantal_wielen;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $aantal_zitplaatsen;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $afstand_hart_koppeling_tot_achterzijde_voertuig;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $afstand_voorzijde_voertuig_tot_hart_koppeling;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $api_gekentekende_voertuigen_assen;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $api_gekentekende_voertuigen_brandstof;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $api_gekentekende_voertuigen_carrosserie;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $api_gekentekende_voertuigen_carrosserie_specifiek;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $api_gekentekende_voertuigen_voertuigklasse;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $breedte;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $bruto_bpm;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $cilinderinhoud;

    /**
     * @var string
     * @Annotation\Type("DateTime")
     */
    private $datum_eerste_afgifte_nederland;

    /**
     * @var string
     * @Annotation\Type("DateTime")
     */
    private $datum_eerste_toelating;

    /**
     * @var string
     * @Annotation\Type("DateTime")
     */
    private $datum_tenaamstelling;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $eerste_kleur;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $europese_voertuigcategorie;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $export_indicator;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $handelsbenaming;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $inrichting;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $kenteken;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $lengte;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $massa_ledig_voertuig;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $massa_rijklaar;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $maximum_massa_trekken_ongeremd;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $maximum_trekken_massa_geremd;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $merk;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $openstaande_terugroepactie_indicator;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $plaats_chassisnummer;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $technische_max_massa_voertuig;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $toegestane_maximum_massa_voertuig;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $tweede_kleur;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $typegoedkeuringsnummer;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $uitvoering;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $variant;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $vermogen_massarijklaar;

    /**
     * @var string
     * @Annotation\Type("DateTime")
     */
    private $vervaldatum_apk;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $voertuigsoort;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $volgnummer_wijziging_eu_typegoedkeuring;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $wacht_op_keuren;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $wam_verzekerd;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $wielbasis;

    /**
     * @var string
     * @Annotation\Type("string")
     */
    private $zuinigheidslabel;

    /**
     * @return string
     */
    public function getAantalCilinders()
    {
        return $this->aantal_cilinders;
    }

    /**
     * @param string $aantal_cilinders
     */
    public function setAantalCilinders($aantal_cilinders)
    {
        $this->aantal_cilinders = $aantal_cilinders;
    }

    /**
     * @return string
     */
    public function getAantalDeuren()
    {
        return $this->aantal_deuren;
    }

    /**
     * @param string $aantal_deuren
     */
    public function setAantalDeuren($aantal_deuren)
    {
        $this->aantal_deuren = $aantal_deuren;
    }

    /**
     * @return string
     */
    public function getAantalWielen()
    {
        return $this->aantal_wielen;
    }

    /**
     * @param string $aantal_wielen
     */
    public function setAantalWielen($aantal_wielen)
    {
        $this->aantal_wielen = $aantal_wielen;
    }

    /**
     * @return string
     */
    public function getAantalZitplaatsen()
    {
        return $this->aantal_zitplaatsen;
    }

    /**
     * @param string $aantal_zitplaatsen
     */
    public function setAantalZitplaatsen($aantal_zitplaatsen)
    {
        $this->aantal_zitplaatsen = $aantal_zitplaatsen;
    }

    /**
     * @return string
     */
    public function getAfstandHartKoppelingTotAchterzijdeVoertuig()
    {
        return $this->afstand_hart_koppeling_tot_achterzijde_voertuig;
    }

    /**
     * @param string $afstand_hart_koppeling_tot_achterzijde_voertuig
     */
    public function setAfstandHartKoppelingTotAchterzijdeVoertuig($afstand_hart_koppeling_tot_achterzijde_voertuig)
    {
        $this->afstand_hart_koppeling_tot_achterzijde_voertuig = $afstand_hart_koppeling_tot_achterzijde_voertuig;
    }

    /**
     * @return string
     */
    public function getAfstandVoorzijdeVoertuigTotHartKoppeling()
    {
        return $this->afstand_voorzijde_voertuig_tot_hart_koppeling;
    }

    /**
     * @param string $afstand_voorzijde_voertuig_tot_hart_koppeling
     */
    public function setAfstandVoorzijdeVoertuigTotHartKoppeling($afstand_voorzijde_voertuig_tot_hart_koppeling)
    {
        $this->afstand_voorzijde_voertuig_tot_hart_koppeling = $afstand_voorzijde_voertuig_tot_hart_koppeling;
    }

    /**
     * @return string
     */
    public function getApiGekentekendeVoertuigenAssen()
    {
        return $this->api_gekentekende_voertuigen_assen;
    }

    /**
     * @param string $api_gekentekende_voertuigen_assen
     */
    public function setApiGekentekendeVoertuigenAssen($api_gekentekende_voertuigen_assen)
    {
        $this->api_gekentekende_voertuigen_assen = $api_gekentekende_voertuigen_assen;
    }

    /**
     * @return string
     */
    public function getApiGekentekendeVoertuigenBrandstof()
    {
        return $this->api_gekentekende_voertuigen_brandstof;
    }

    /**
     * @param string $api_gekentekende_voertuigen_brandstof
     */
    public function setApiGekentekendeVoertuigenBrandstof($api_gekentekende_voertuigen_brandstof)
    {
        $this->api_gekentekende_voertuigen_brandstof = $api_gekentekende_voertuigen_brandstof;
    }

    /**
     * @return string
     */
    public function getApiGekentekendeVoertuigenCarrosserie()
    {
        return $this->api_gekentekende_voertuigen_carrosserie;
    }

    /**
     * @param string $api_gekentekende_voertuigen_carrosserie
     */
    public function setApiGekentekendeVoertuigenCarrosserie($api_gekentekende_voertuigen_carrosserie)
    {
        $this->api_gekentekende_voertuigen_carrosserie = $api_gekentekende_voertuigen_carrosserie;
    }

    /**
     * @return string
     */
    public function getApiGekentekendeVoertuigenCarrosserieSpecifiek()
    {
        return $this->api_gekentekende_voertuigen_carrosserie_specifiek;
    }

    /**
     * @param string $api_gekentekende_voertuigen_carrosserie_specifiek
     */
    public function setApiGekentekendeVoertuigenCarrosserieSpecifiek($api_gekentekende_voertuigen_carrosserie_specifiek)
    {
        $this->api_gekentekende_voertuigen_carrosserie_specifiek = $api_gekentekende_voertuigen_carrosserie_specifiek;
    }

    /**
     * @return string
     */
    public function getApiGekentekendeVoertuigenVoertuigklasse()
    {
        return $this->api_gekentekende_voertuigen_voertuigklasse;
    }

    /**
     * @param string $api_gekentekende_voertuigen_voertuigklasse
     */
    public function setApiGekentekendeVoertuigenVoertuigklasse($api_gekentekende_voertuigen_voertuigklasse)
    {
        $this->api_gekentekende_voertuigen_voertuigklasse = $api_gekentekende_voertuigen_voertuigklasse;
    }

    /**
     * @return string
     */
    public function getBreedte()
    {
        return $this->breedte;
    }

    /**
     * @param string $breedte
     */
    public function setBreedte($breedte)
    {
        $this->breedte = $breedte;
    }

    /**
     * @return string
     */
    public function getBrutoBpm()
    {
        return $this->bruto_bpm;
    }

    /**
     * @param string $bruto_bpm
     */
    public function setBrutoBpm($bruto_bpm)
    {
        $this->bruto_bpm = $bruto_bpm;
    }

    /**
     * @return string
     */
    public function getCilinderinhoud()
    {
        return $this->cilinderinhoud;
    }

    /**
     * @param string $cilinderinhoud
     */
    public function setCilinderinhoud($cilinderinhoud)
    {
        $this->cilinderinhoud = $cilinderinhoud;
    }

    /**
     * @return string
     */
    public function getDatumEersteAfgifteNederland()
    {
        return $this->datum_eerste_afgifte_nederland;
    }

    /**
     * @param string $datum_eerste_afgifte_nederland
     */
    public function setDatumEersteAfgifteNederland($datum_eerste_afgifte_nederland)
    {
        $this->datum_eerste_afgifte_nederland = $datum_eerste_afgifte_nederland;
    }

    /**
     * @return string
     */
    public function getDatumEersteToelating()
    {
        return $this->datum_eerste_toelating;
    }

    /**
     * @param string $datum_eerste_toelating
     */
    public function setDatumEersteToelating($datum_eerste_toelating)
    {
        $this->datum_eerste_toelating = $datum_eerste_toelating;
    }

    /**
     * @return string
     */
    public function getDatumTenaamstelling()
    {
        return $this->datum_tenaamstelling;
    }

    /**
     * @param string $datum_tenaamstelling
     */
    public function setDatumTenaamstelling($datum_tenaamstelling)
    {
        $this->datum_tenaamstelling = $datum_tenaamstelling;
    }

    /**
     * @return string
     */
    public function getEersteKleur()
    {
        return $this->eerste_kleur;
    }

    /**
     * @param string $eerste_kleur
     */
    public function setEersteKleur($eerste_kleur)
    {
        $this->eerste_kleur = $eerste_kleur;
    }

    /**
     * @return string
     */
    public function getEuropeseVoertuigcategorie()
    {
        return $this->europese_voertuigcategorie;
    }

    /**
     * @param string $europese_voertuigcategorie
     */
    public function setEuropeseVoertuigcategorie($europese_voertuigcategorie)
    {
        $this->europese_voertuigcategorie = $europese_voertuigcategorie;
    }

    /**
     * @return string
     */
    public function getExportIndicator()
    {
        return $this->export_indicator;
    }

    /**
     * @param string $export_indicator
     */
    public function setExportIndicator($export_indicator)
    {
        $this->export_indicator = $export_indicator;
    }

    /**
     * @return string
     */
    public function getHandelsbenaming()
    {
        return $this->handelsbenaming;
    }

    /**
     * @param string $handelsbenaming
     */
    public function setHandelsbenaming($handelsbenaming)
    {
        $this->handelsbenaming = $handelsbenaming;
    }

    /**
     * @return string
     */
    public function getInrichting()
    {
        return $this->inrichting;
    }

    /**
     * @param string $inrichting
     */
    public function setInrichting($inrichting)
    {
        $this->inrichting = $inrichting;
    }

    /**
     * @return string
     */
    public function getKenteken()
    {
        return $this->kenteken;
    }

    /**
     * @param string $kenteken
     */
    public function setKenteken($kenteken)
    {
        $this->kenteken = $kenteken;
    }

    /**
     * @return string
     */
    public function getLengte()
    {
        return $this->lengte;
    }

    /**
     * @param string $lengte
     */
    public function setLengte($lengte)
    {
        $this->lengte = $lengte;
    }

    /**
     * @return string
     */
    public function getMassaLedigVoertuig()
    {
        return $this->massa_ledig_voertuig;
    }

    /**
     * @param string $massa_ledig_voertuig
     */
    public function setMassaLedigVoertuig($massa_ledig_voertuig)
    {
        $this->massa_ledig_voertuig = $massa_ledig_voertuig;
    }

    /**
     * @return string
     */
    public function getMassaRijklaar()
    {
        return $this->massa_rijklaar;
    }

    /**
     * @param string $massa_rijklaar
     */
    public function setMassaRijklaar($massa_rijklaar)
    {
        $this->massa_rijklaar = $massa_rijklaar;
    }

    /**
     * @return string
     */
    public function getMaximumMassaTrekkenOngeremd()
    {
        return $this->maximum_massa_trekken_ongeremd;
    }

    /**
     * @param string $maximum_massa_trekken_ongeremd
     */
    public function setMaximumMassaTrekkenOngeremd($maximum_massa_trekken_ongeremd)
    {
        $this->maximum_massa_trekken_ongeremd = $maximum_massa_trekken_ongeremd;
    }

    /**
     * @return string
     */
    public function getMaximumTrekkenMassaGeremd()
    {
        return $this->maximum_trekken_massa_geremd;
    }

    /**
     * @param string $maximum_trekken_massa_geremd
     */
    public function setMaximumTrekkenMassaGeremd($maximum_trekken_massa_geremd)
    {
        $this->maximum_trekken_massa_geremd = $maximum_trekken_massa_geremd;
    }

    /**
     * @return string
     */
    public function getMerk()
    {
        return $this->merk;
    }

    /**
     * @param string $merk
     */
    public function setMerk($merk)
    {
        $this->merk = $merk;
    }

    /**
     * @return string
     */
    public function getOpenstaandeTerugroepactieIndicator()
    {
        return $this->openstaande_terugroepactie_indicator;
    }

    /**
     * @param string $openstaande_terugroepactie_indicator
     */
    public function setOpenstaandeTerugroepactieIndicator($openstaande_terugroepactie_indicator)
    {
        $this->openstaande_terugroepactie_indicator = $openstaande_terugroepactie_indicator;
    }

    /**
     * @return string
     */
    public function getPlaatsChassisnummer()
    {
        return $this->plaats_chassisnummer;
    }

    /**
     * @param string $plaats_chassisnummer
     */
    public function setPlaatsChassisnummer($plaats_chassisnummer)
    {
        $this->plaats_chassisnummer = $plaats_chassisnummer;
    }

    /**
     * @return string
     */
    public function getTechnischeMaxMassaVoertuig()
    {
        return $this->technische_max_massa_voertuig;
    }

    /**
     * @param string $technische_max_massa_voertuig
     */
    public function setTechnischeMaxMassaVoertuig($technische_max_massa_voertuig)
    {
        $this->technische_max_massa_voertuig = $technische_max_massa_voertuig;
    }

    /**
     * @return string
     */
    public function getToegestaneMaximumMassaVoertuig()
    {
        return $this->toegestane_maximum_massa_voertuig;
    }

    /**
     * @param string $toegestane_maximum_massa_voertuig
     */
    public function setToegestaneMaximumMassaVoertuig($toegestane_maximum_massa_voertuig)
    {
        $this->toegestane_maximum_massa_voertuig = $toegestane_maximum_massa_voertuig;
    }

    /**
     * @return string
     */
    public function getTweedeKleur()
    {
        return $this->tweede_kleur;
    }

    /**
     * @param string $tweede_kleur
     */
    public function setTweedeKleur($tweede_kleur)
    {
        $this->tweede_kleur = $tweede_kleur;
    }

    /**
     * @return string
     */
    public function getTypegoedkeuringsnummer()
    {
        return $this->typegoedkeuringsnummer;
    }

    /**
     * @param string $typegoedkeuringsnummer
     */
    public function setTypegoedkeuringsnummer($typegoedkeuringsnummer)
    {
        $this->typegoedkeuringsnummer = $typegoedkeuringsnummer;
    }

    /**
     * @return string
     */
    public function getUitvoering()
    {
        return $this->uitvoering;
    }

    /**
     * @param string $uitvoering
     */
    public function setUitvoering($uitvoering)
    {
        $this->uitvoering = $uitvoering;
    }

    /**
     * @return string
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * @param string $variant
     */
    public function setVariant($variant)
    {
        $this->variant = $variant;
    }

    /**
     * @return string
     */
    public function getVermogenMassarijklaar()
    {
        return $this->vermogen_massarijklaar;
    }

    /**
     * @param string $vermogen_massarijklaar
     */
    public function setVermogenMassarijklaar($vermogen_massarijklaar)
    {
        $this->vermogen_massarijklaar = $vermogen_massarijklaar;
    }

    /**
     * @return string
     */
    public function getVervaldatumApk()
    {
        return $this->vervaldatum_apk;
    }

    /**
     * @param string $vervaldatum_apk
     */
    public function setVervaldatumApk($vervaldatum_apk)
    {
        $this->vervaldatum_apk = $vervaldatum_apk;
    }

    /**
     * @return string
     */
    public function getVoertuigsoort()
    {
        return $this->voertuigsoort;
    }

    /**
     * @param string $voertuigsoort
     */
    public function setVoertuigsoort($voertuigsoort)
    {
        $this->voertuigsoort = $voertuigsoort;
    }

    /**
     * @return string
     */
    public function getVolgnummerWijzigingEuTypegoedkeuring()
    {
        return $this->volgnummer_wijziging_eu_typegoedkeuring;
    }

    /**
     * @param string $volgnummer_wijziging_eu_typegoedkeuring
     */
    public function setVolgnummerWijzigingEuTypegoedkeuring($volgnummer_wijziging_eu_typegoedkeuring)
    {
        $this->volgnummer_wijziging_eu_typegoedkeuring = $volgnummer_wijziging_eu_typegoedkeuring;
    }

    /**
     * @return string
     */
    public function getWachtOpKeuren()
    {
        return $this->wacht_op_keuren;
    }

    /**
     * @param string $wacht_op_keuren
     */
    public function setWachtOpKeuren($wacht_op_keuren)
    {
        $this->wacht_op_keuren = $wacht_op_keuren;
    }

    /**
     * @return string
     */
    public function getWamVerzekerd()
    {
        return $this->wam_verzekerd;
    }

    /**
     * @param string $wam_verzekerd
     */
    public function setWamVerzekerd($wam_verzekerd)
    {
        $this->wam_verzekerd = $wam_verzekerd;
    }

    /**
     * @return string
     */
    public function getWielbasis()
    {
        return $this->wielbasis;
    }

    /**
     * @param string $wielbasis
     */
    public function setWielbasis($wielbasis)
    {
        $this->wielbasis = $wielbasis;
    }

    /**
     * @return string
     */
    public function getZuinigheidslabel()
    {
        return $this->zuinigheidslabel;
    }

    /**
     * @param string $zuinigheidslabel
     */
    public function setZuinigheidslabel($zuinigheidslabel)
    {
        $this->zuinigheidslabel = $zuinigheidslabel;
    }


}
