<?php

namespace MijnsubclubBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser implements ORMBehaviors\Tree\NodeInterface, \ArrayAccess
{
    use ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\Geocodable\Geocodable,
        ORMBehaviors\Loggable\Loggable,
        ORMBehaviors\SoftDeletable\SoftDeletable,
        ORMBehaviors\Sortable\Sortable,
        ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable,
        ORMBehaviors\Tree\Node;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="MijnsubclubBundle\Entity\Group")
     * @ORM\JoinTable(name="fos_user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Voornaam.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=1,
     *     max=255,
     *     minMessage="Voornaam is te kort",
     *     maxMessage="Voornaam is te lang",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $voornaam;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Achternaam.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="Achternaam is te kort",
     *     maxMessage="Achternaam is te lang",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $achternaam;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $tussenvoegsel;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lidnummer;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\Length(
     *     min=0,
     *     max=255,
     *     maxMessage="Forumnaam is te lang",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $forumnaam;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $straatennummer;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $postcode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $plaats;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $land;

    /**
     * @ORM\OneToMany(targetEntity="Voertuig", mappedBy="user")
     */
    private $voertuigen;

    /**
     * @ORM\OneToMany(targetEntity="MijnsubclubBundle\Entity\Transactie", mappedBy="user")
     */
    protected $transacties;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->transacties = new ArrayCollection();
        $this->voertuigen = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getVoornaam()
    {
        return $this->voornaam;
    }

    /**
     * @param mixed $voornaam
     */
    public function setVoornaam($voornaam)
    {
        $this->voornaam = $voornaam;
    }

    /**
     * @return mixed
     */
    public function getAchternaam()
    {
        return $this->achternaam;
    }

    /**
     * @param mixed $achternaam
     */
    public function setAchternaam($achternaam)
    {
        $this->achternaam = $achternaam;
    }

    /**
     * @return mixed
     */
    public function getTussenvoegsel()
    {
        return $this->tussenvoegsel;
    }

    /**
     * @param mixed $tussenvoegsel
     */
    public function setTussenvoegsel($tussenvoegsel)
    {
        $this->tussenvoegsel = $tussenvoegsel;
    }

    /**
     * @return mixed
     */
    public function getLidnummer()
    {
        return $this->lidnummer;
    }

    /**
     * @param mixed $lidnummer
     */
    public function setLidnummer($lidnummer)
    {
        $this->lidnummer = $lidnummer;
    }

    /**
     * @return mixed
     */
    public function getForumnaam()
    {
        return $this->forumnaam;
    }

    /**
     * @param mixed $forumnaam
     */
    public function setForumnaam($forumnaam)
    {
        $this->forumnaam = $forumnaam;
    }

    /**
     * @return string
     */
    public function getStraatennummer()
    {
        return $this->straatennummer;
    }

    /**
     * @param string $straatennummer
     */
    public function setStraatennummer($straatennummer)
    {
        $this->straatennummer = $straatennummer;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getPlaats()
    {
        return $this->plaats;
    }

    /**
     * @param string $plaats
     */
    public function setPlaats($plaats)
    {
        $this->plaats = $plaats;
    }

    /**
     * @return string
     */
    public function getLand()
    {
        return $this->land;
    }

    /**
     * @param string $land
     */
    public function setLand($land)
    {
        $this->land = $land;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->tussenvoegsel .' '. $this->achternaam.', '. $this->voornaam;
    }

    /**
     * Add voertuigen
     *
     * @param Voertuig $voertuigen
     *
     * @return User
     */
    public function addVoertuigen(Voertuig $voertuigen)
    {
        $this->voertuigen[] = $voertuigen;

        return $this;
    }

    /**
     * Remove voertuigen
     *
     * @param Voertuig $voertuigen
     */
    public function removeVoertuigen(Voertuig $voertuigen)
    {
        $this->voertuigen->removeElement($voertuigen);
    }

    /**
     * Get voertuigen
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVoertuigen()
    {
        return $this->voertuigen;
    }

    /**
     * @return mixed
     */
    public function getTransacties()
    {
        return $this->transacties;
    }

    /**
     * @param mixed $transacties
     */
    public function setTransacties($transacties)
    {
        $this->transacties = $transacties;
    }


}
