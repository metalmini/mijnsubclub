<?php

namespace MijnsubclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


/**
 * Verbruik
 *
 * @ORM\Table(name="verbruik")
 * @ORM\Entity(repositoryClass="MijnsubclubBundle\Repository\VerbruikRepository")
 */
class Verbruik implements ORMBehaviors\Tree\NodeInterface, \ArrayAccess
{
    use ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\Geocodable\Geocodable,
        ORMBehaviors\Loggable\Loggable,
        ORMBehaviors\SoftDeletable\SoftDeletable,
        ORMBehaviors\Sortable\Sortable,
        ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable,
        ORMBehaviors\Tree\Node;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var number
     *
     * @ORM\Column(name="tellerstand", type="decimal")
     */
    private $tellerstand;

    /**
     * @var number
     *
     * @ORM\Column(name="liter", type="decimal", precision=8, scale=2)
     */
    private $liter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="getankedOp", type="datetime", nullable=true)
     */
    private $getankedOp;

    /**
     * @var string
     *
     * @ORM\Column(name="totaalbedrag", type="decimal", precision=8, scale=2, nullable=true)
     */
    private $totaalbedrag;

    /**
     * @var string
     *
     * @ORM\Column(name="soortbrandstof", type="string", length=255)
     */
    private $soortbrandstof;

    /**
     * @var string
     *
     * @ORM\Column(name="bijzonderheden", type="string", length=255, nullable=true)
     */
    private $bijzonderheden;

    /**
     * @var number
     *
     * @ORM\Column(name="tellerstandverschil", type="decimal", nullable=true)
     */
    private $tellerstandverschil;

    /**
     * @ORM\ManyToOne(targetEntity="Voertuig", inversedBy="verbruiken")
     * @ORM\JoinColumn(name="voertuig_id", referencedColumnName="id")
     */
    private $voertuig;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tellerstand
     *
     * @param $tellerstand
     *
     * @return Verbruik
     */
    public function setTellerstand($tellerstand)
    {
        $this->tellerstand = $tellerstand;

        return $this;
    }

    /**
     * Get tellerstand
     *
     * @return string
     */
    public function getTellerstand()
    {
        return $this->tellerstand;
    }

    /**
     * @return number
     */
    public function getLiter()
    {
        return $this->liter;
    }

    /**
     * @param number $liter
     */
    public function setLiter($liter)
    {
        $this->liter = $liter;
    }

    /**
     * @return \DateTime
     */
    public function getGetankedOp()
    {
        return $this->getankedOp;
    }

    /**
     * @param \DateTime $getankedOp
     */
    public function setGetankedOp($getankedOp)
    {
        $this->getankedOp = $getankedOp;
    }

    /**
     * Set totaalbedrag
     *
     * @param string $totaalbedrag
     *
     * @return Verbruik
     */
    public function setTotaalbedrag($totaalbedrag)
    {
        $this->totaalbedrag = $totaalbedrag;

        return $this;
    }

    /**
     * Get totaalbedrag
     *
     * @return string
     */
    public function getTotaalbedrag()
    {
        return $this->totaalbedrag;
    }

    /**
     * @return mixed
     */
    public function getSoortbrandstof()
    {
        return $this->soortbrandstof;
    }

    /**
     * @param mixed $soortbrandstof
     */
    public function setSoortbrandstof($soortbrandstof)
    {
        $this->soortbrandstof = $soortbrandstof;
    }

    /**
     * @return mixed
     */
    public function getBijzonderheden()
    {
        return $this->bijzonderheden;
    }

    /**
     * @param mixed $bijzonderheden
     */
    public function setBijzonderheden($bijzonderheden)
    {
        $this->bijzonderheden = $bijzonderheden;
    }

    /**
     * @return mixed
     */
    public function getTellerstandverschil()
    {
        return $this->tellerstandverschil;
    }

    /**
     * @param mixed $tellerstandverschil
     */
    public function setTellerstandverschil($tellerstandverschil)
    {
        $this->tellerstandverschil = $tellerstandverschil;
    }

    /**
     * @return mixed
     */
    public function getVoertuig()
    {
        return $this->voertuig;
    }

    /**
     * @param mixed $voertuig
     */
    public function setVoertuig($voertuig)
    {
        $this->voertuig = $voertuig;
    }
}

