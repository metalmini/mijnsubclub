<?php

namespace MijnsubclubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


/**
 * Transactie
 *
 * @ORM\Table(name="transactie")
 * @ORM\Entity(repositoryClass="MijnsubclubBundle\Repository\TransactieRepository")
 */
class Transactie implements ORMBehaviors\Tree\NodeInterface, \ArrayAccess
{
    use ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\Geocodable\Geocodable,
        ORMBehaviors\Loggable\Loggable,
        ORMBehaviors\SoftDeletable\SoftDeletable,
        ORMBehaviors\Sortable\Sortable,
        ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable,
        ORMBehaviors\Tree\Node;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="betaald", type="boolean")
     */
    private $betaald;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="betaaldOp", type="datetime")
     */
    private $betaaldOp;

    /**
     * @var string
     *
     * @ORM\Column(name="opmerking", type="string", length=255, nullable=true)
     */
    private $opmerking;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="transacties")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Lidmaatschapsjaar", inversedBy="lidmaatschapsjaren")
     * @ORM\JoinColumn(name="lidmaatschapsjaar_id", referencedColumnName="id")
     */
    private $lidmaatschapsjaar;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set betaald
     *
     * @param boolean $betaald
     *
     * @return Transactie
     */
    public function setBetaald($betaald)
    {
        $this->betaald = $betaald;

        return $this;
    }

    /**
     * Get betaald
     *
     * @return bool
     */
    public function getBetaald()
    {
        return $this->betaald;
    }

    /**
     * Set betaaldOp
     *
     * @param \DateTime $betaaldOp
     *
     * @return Transactie
     */
    public function setBetaaldOp($betaaldOp)
    {
        $this->betaaldOp = $betaaldOp;

        return $this;
    }

    /**
     * Get betaaldOp
     *
     * @return \DateTime
     */
    public function getBetaaldOp()
    {
        return $this->betaaldOp;
    }

    /**
     * Set opmerking
     *
     * @param string $opmerking
     *
     * @return Transactie
     */
    public function setOpmerking($opmerking)
    {
        $this->opmerking = $opmerking;

        return $this;
    }

    /**
     * Get opmerking
     *
     * @return string
     */
    public function getOpmerking()
    {
        return $this->opmerking;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getLidmaatschapsjaar()
    {
        return $this->lidmaatschapsjaar;
    }

    /**
     * @param mixed $lidmaatschapsjaar
     */
    public function setLidmaatschapsjaar($lidmaatschapsjaar)
    {
        $this->lidmaatschapsjaar = $lidmaatschapsjaar;
    }


}

