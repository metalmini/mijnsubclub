<?php

namespace MijnsubclubBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


/**
 * Lidmaatschapsjaar
 *
 * @ORM\Table(name="lidmaatschapsjaar")
 * @ORM\Entity(repositoryClass="MijnsubclubBundle\Repository\LidmaatschapsjaarRepository")
 */
class Lidmaatschapsjaar implements ORMBehaviors\Tree\NodeInterface, \ArrayAccess
{
    use ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\Geocodable\Geocodable,
        ORMBehaviors\Loggable\Loggable,
        ORMBehaviors\SoftDeletable\SoftDeletable,
        ORMBehaviors\Sortable\Sortable,
        ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable,
        ORMBehaviors\Tree\Node;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jaar", type="date", unique=true)
     */
    private $jaar;

    /**
     * @var string
     *
     * @ORM\Column(name="contributie", type="string", length=255)
     */
    private $contributie;

    /**
     * @var string
     *
     * @ORM\Column(name="inschrijfgeld", type="string", length=255)
     */
    private $inschrijfgeld;

    /**
     * @ORM\OneToMany(targetEntity="MijnsubclubBundle\Entity\Transactie", mappedBy="lidmaatschapsjaar")
     */
    protected $transacties;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->transacties = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jaar
     *
     * @param \DateTime $jaar
     *
     * @return Lidmaatschapsjaar
     */
    public function setJaar($jaar)
    {
        $this->jaar = $jaar;

        return $this;
    }

    /**
     * Get jaar
     *
     * @return \DateTime
     */
    public function getJaar()
    {
        return $this->jaar;
    }

    /**
     * Set contributie
     *
     * @param string $contributie
     *
     * @return Lidmaatschapsjaar
     */
    public function setContributie($contributie)
    {
        $this->contributie = $contributie;

        return $this;
    }

    /**
     * Get contributie
     *
     * @return string
     */
    public function getContributie()
    {
        return $this->contributie;
    }

    /**
     * Set inschrijfgeld
     *
     * @param string $inschrijfgeld
     *
     * @return Lidmaatschapsjaar
     */
    public function setInschrijfgeld($inschrijfgeld)
    {
        $this->inschrijfgeld = $inschrijfgeld;

        return $this;
    }

    /**
     * Get inschrijfgeld
     *
     * @return string
     */
    public function getInschrijfgeld()
    {
        return $this->inschrijfgeld;
    }

    /**
     * @return mixed
     */
    public function getTransacties()
    {
        return $this->transacties;
    }

    /**
     * @param mixed $transacties
     */
    public function setTransacties($transacties)
    {
        $this->transacties = $transacties;
    }

    public function __toString()
    {
        $date = $this->getJaar();
        return $date->format('Y');
    }
}

