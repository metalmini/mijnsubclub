<?php

namespace MijnsubclubBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Voertuig
 *
 * @ORM\Table(name="voertuig")
 * @ORM\Entity(repositoryClass="MijnsubclubBundle\Repository\VoertuigRepository")
 */
class Voertuig implements ORMBehaviors\Tree\NodeInterface, \ArrayAccess
{
    use ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\Geocodable\Geocodable,
        ORMBehaviors\Loggable\Loggable,
        ORMBehaviors\SoftDeletable\SoftDeletable,
        ORMBehaviors\Sortable\Sortable,
        ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable,
        ORMBehaviors\Tree\Node;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="naam", type="string", length=255, nullable=true)
     */
    private $naam;

    /**
     * @var string
     *
     * @ORM\Column(name="merk", type="string", length=255)
     */
    private $merk;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=255)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="bouwjaar", type="string", length=255, nullable=true)
     */
    private $bouwjaar;

    /**
     * @var string
     *
     * @ORM\Column(name="kenteken", type="string", length=255)
     */
    private $kenteken;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="voertuigen")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Verbruik", mappedBy="voertuig")
     */
    private $verbruiken;

    public function __construct()
    {
        $this->verbruiken = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * @param string $naam
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Voertuig
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Voertuig
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set bouwjaar
     *
     * @param string $bouwjaar
     *
     * @return Voertuig
     */
    public function setBouwjaar($bouwjaar)
    {
        $this->bouwjaar = $bouwjaar;

        return $this;
    }

    /**
     * Get bouwjaar
     *
     * @return string
     */
    public function getBouwjaar()
    {
        return $this->bouwjaar;
    }

    /**
     * Set kenteken
     *
     * @param string $kenteken
     *
     * @return Voertuig
     */
    public function setKenteken($kenteken)
    {
        $this->kenteken = $kenteken;

        return $this;
    }

    /**
     * Get kenteken
     *
     * @return string
     */
    public function getKenteken()
    {
        return $this->kenteken;
    }

    /**
     * @return string
     */
    public function getMerk()
    {
        return $this->merk;
    }

    /**
     * @param string $merk
     */
    public function setMerk($merk)
    {
        $this->merk = $merk;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Voertuig
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Add verbruik
     *
     * @param Verbruik $verbruiken
     *
     * @return Voertuig
     */
    public function addVerbruiken(Verbruik $verbruiken)
    {
        $this->verbruiken[] = $verbruiken;

        return $this;
    }

    /**
     * Remove voertuigen
     *
     * @param Verbruik $verbruiken
     */
    public function removeVoertuigen(Verbruik $verbruiken)
    {
        $this->verbruiken->removeElement($verbruiken);
    }

    /**
     * Get verbruiken
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVerbruiken()
    {
        return $this->verbruiken;
    }
}
