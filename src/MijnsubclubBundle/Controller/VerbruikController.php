<?php

namespace MijnsubclubBundle\Controller;

use MijnsubclubBundle\Entity\Verbruik;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Verbruik controller.
 *
 * @Route("/voertuig/{voertuig_id}/verbruik")
 */
class VerbruikController extends Controller
{
    /**
     * Lists all verbruik entities.
     *
     * @Route("/", name="verbruik_index")
     * @Method("GET")
     * @param $voertuig_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($voertuig_id)
    {
        $em = $this->getDoctrine()->getManager();
        $voertuig = $em->getRepository('MijnsubclubBundle:Voertuig')->find($voertuig_id);
        if ($this->getUser() !== $voertuig->getUser() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->denyAccessUnlessGranted('ROLE_EDIT', $voertuig, 'You cannot edit this item.');
        }

        $verbruiksoverzicht = $em->getRepository('MijnsubclubBundle:Verbruik')->findAllByVoertuigFilterd($voertuig->getId());

        return $this->render(
            '@Mijnsubclub/Verbruik/index.html.twig',
            array(
                'voertuig' => $voertuig,
                'verbruiksoverzicht' => $verbruiksoverzicht,
            )
        );
    }

    /**
     * Creates a new verbruik entity.
     *
     * @Route("/new", name="verbruik_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $voertuig_id)
    {
        $em = $this->getDoctrine()->getManager();
        $voertuig = $em->getRepository('MijnsubclubBundle:Voertuig')->find($voertuig_id);
        if ($this->getUser() !== $voertuig->getUser() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->denyAccessUnlessGranted('ROLE_EDIT', $voertuig, 'You cannot edit this item.');
        }

        $verbruik = new Verbruik();
        $verbruik->setVoertuig($voertuig);

        $form = $this->createForm('MijnsubclubBundle\Form\Type\VerbruikType', $verbruik)
            ->add('Opslaan', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($verbruik);
            $em->flush();

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render(
            '@Mijnsubclub/Verbruik/new.html.twig',
            array(
                'voertuig' => $voertuig,
                'verbruik' => $verbruik,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a verbruik entity.
     *
     * @Route("/{id}", name="verbruik_show")
     * @Method("GET")
     */
    public function showAction(Verbruik $verbruik, $voertuig_id)
    {
        $em = $this->getDoctrine()->getManager();
        $voertuig = $em->getRepository('MijnsubclubBundle:Voertuig')->find($voertuig_id);
        if ($this->getUser() !== $voertuig->getUser() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->denyAccessUnlessGranted('ROLE_EDIT', $voertuig, 'You cannot edit this item.');
        }

        $deleteForm = $this->createDeleteForm($verbruik, $voertuig_id);

        return $this->render(
            '@Mijnsubclub/Verbruik/show.html.twig',
            array(
                'verbruik' => $verbruik,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing verbruik entity.
     *
     * @Route("/{id}/edit", name="verbruik_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Verbruik $verbruik, $voertuig_id)
    {
        $em = $this->getDoctrine()->getManager();
        $voertuig = $em->getRepository('MijnsubclubBundle:Voertuig')->find($voertuig_id);

        if ($this->getUser() !== $voertuig->getUser() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->denyAccessUnlessGranted('ROLE_EDIT', $verbruik, 'You cannot edit this item.');
        }

        $deleteForm = $this->createDeleteForm($verbruik, $voertuig_id);
        $editForm = $this->createForm('MijnsubclubBundle\Form\Type\VerbruikType', $verbruik)->add(
            'Opslaan',
            SubmitType::class
        );
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em->flush();

            return $this->redirectToRoute('verbruik_edit', array('id' => $verbruik->getId(), 'voertuig_id' => $voertuig_id));
        }

        return $this->render(
            '@Mijnsubclub/Verbruik/edit.html.twig',
            array(
                'voertuig' => $voertuig,
                'verbruik' => $verbruik,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a verbruik entity.
     *
     * @Route("/{id}", name="verbruik_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Verbruik $verbruik, $voertuig_id)
    {
        $em = $this->getDoctrine()->getManager();
        $voertuig = $em->getRepository('MijnsubclubBundle:Voertuig')->find($voertuig_id);
        if ($this->getUser() !== $voertuig->getUser() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->denyAccessUnlessGranted('ROLE_EDIT', $verbruik, 'You cannot edit this item.');
        }

        $form = $this->createDeleteForm($verbruik, $voertuig_id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($verbruik);
            $em->flush();
        }

        return $this->redirectToRoute('fos_user_profile_show');
    }

    /**
     * Creates a form to delete a verbruik entity.
     *
     * @param Verbruik $verbruik The verbruik entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Verbruik $verbruik, $voertuig_id)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl('verbruik_delete', array('id' => $verbruik->getId(), 'voertuig_id' => $voertuig_id))
            )
            ->setMethod('DELETE')
            ->getForm();
    }
}
