<?php

namespace MijnsubclubBundle\Controller;

use MijnsubclubBundle\Entity\Lidmaatschapsjaar;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Lidmaatschapsjaar controller.
 */
class LidmaatschapsjaarController extends Controller
{
    /**
     * Lists all lidmaatschapsjaar entities.
     *
     * @Route("/admin/lidmaatschapsjaren", name="admin_lidmaatschapsjaar_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $lidmaatschapsjaren = $em->getRepository('MijnsubclubBundle:Lidmaatschapsjaar')->findAll();

        return $this->render(
            '@Mijnsubclub/lidmaatschapsjaar/index.html.twig',
            array(
                'lidmaatschapsjaren' => $lidmaatschapsjaren,
            )
        );
    }

    /**
     * Creates a new lidmaatschapsjaar entity.
     *
     * @Route("/admin/lidmaatschapsjaar/new", name="admin_lidmaatschapsjaar_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $lidmaatschapsjaar = new Lidmaatschapsjaar();
        $form = $this->createForm('MijnsubclubBundle\Form\Type\LidmaatschapsjaarType', $lidmaatschapsjaar)
            ->add('Opslaan', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($lidmaatschapsjaar);
            $em->flush();

            return $this->redirectToRoute('admin_lidmaatschapsjaar_show', array('id' => $lidmaatschapsjaar->getId()));
        }

        return $this->render(
            '@Mijnsubclub/lidmaatschapsjaar/new.html.twig',
            array(
                'lidmaatschapsjaar' => $lidmaatschapsjaar,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a lidmaatschapsjaar entity.
     *
     * @Route("/admin/lidmaatschapsjaar/{id}", name="admin_lidmaatschapsjaar_show")
     * @Method("GET")
     * @param Lidmaatschapsjaar $lidmaatschapsjaar
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Lidmaatschapsjaar $lidmaatschapsjaar)
    {
        $deleteForm = $this->createDeleteForm($lidmaatschapsjaar);

        return $this->render(
            '@Mijnsubclub/lidmaatschapsjaar/show.html.twig',
            array(
                'lidmaatschapsjaar' => $lidmaatschapsjaar,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing lidmaatschapsjaar entity.
     *
     * @Route("/admin/lidmaatschapsjaar/{id}/edit", name="admin_lidmaatschapsjaar_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Lidmaatschapsjaar $lidmaatschapsjaar
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Lidmaatschapsjaar $lidmaatschapsjaar)
    {
        $deleteForm = $this->createDeleteForm($lidmaatschapsjaar);
        $editForm = $this->createForm('MijnsubclubBundle\Form\Type\LidmaatschapsjaarType', $lidmaatschapsjaar);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_lidmaatschapsjaar_edit', array('id' => $lidmaatschapsjaar->getId()));
        }

        return $this->render(
            '@Mijnsubclub/lidmaatschapsjaar/edit.html.twig',
            array(
                'lidmaatschapsjaar' => $lidmaatschapsjaar,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a lidmaatschapsjaar entity.
     *
     * @Route("/admin/lidmaatschapsjaar/{id}", name="admin_lidmaatschapsjaar_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Lidmaatschapsjaar $lidmaatschapsjaar
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Lidmaatschapsjaar $lidmaatschapsjaar)
    {
        $form = $this->createDeleteForm($lidmaatschapsjaar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($lidmaatschapsjaar);
            $em->flush();
        }

        return $this->redirectToRoute('admin_lidmaatschapsjaar_index');
    }

    /**
     * Creates a form to delete a lidmaatschapsjaar entity.
     *
     * @param Lidmaatschapsjaar $lidmaatschapsjaar The lidmaatschapsjaar entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Lidmaatschapsjaar $lidmaatschapsjaar)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl('admin_lidmaatschapsjaar_delete', array('id' => $lidmaatschapsjaar->getId()))
            )
            ->setMethod('DELETE')
            ->getForm();
    }
}
