<?php

namespace MijnsubclubBundle\Controller;

use DateTime;
use FOS\UserBundle\Event\FilterGroupResponseEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


class UserController extends Controller
{
    /**
     * @Route("/admin/leden", name="leden_lijst")
     */
    public function indexAction()
    {
        $userManager = $this->get('fos_user.user_manager');
        $lijst = $userManager->findUsers();

        return $this->render('MijnsubclubBundle:user:index.html.twig', array('users' => $lijst));
    }

    /**
     * @Route("/admin/lid/{id}/edit", name="lid_aanpassen")
     * @param Request $request
     * @param $id
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->container->get('fos_user.profile.form.factory');
        /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
        $tokenGenerator = $this->get('fos_user.util.token_generator');
        $userToken = $tokenGenerator->generateToken();

        $user = $userManager->findUserBy(array('id' => $id));

        $form = $formFactory->createForm();
        $form->setData($user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $userManager->updateUser($user);

            $url = $this->generateUrl('leden_lijst');
            $response = new RedirectResponse($url);

            return $response;
        }

        return $this->render('MijnsubclubBundle:user:edit.html.twig', array(
            'id' => $id,
            'user' => $user,
            'token' => $userToken,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/lid/{id}", name="lid_detail", requirements={"id" = "\d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction($id)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));

        return $this->render('MijnsubclubBundle:user:detail.html.twig', array('user' => $user));
    }

    /**
     * @Route("/admin/lid/nieuw", name="lid_nieuw")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        /** @var $groupManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
        $tokenGenerator = $this->get('fos_user.util.token_generator');

        $user = $userManager->createUser();
        $user->setPlainPassword('testing1234');

        $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_INITIALIZE, new UserEvent($user, $request));

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setConfirmationToken($tokenGenerator->generateToken());
            $user->setPasswordRequestedAt(new DateTime());
            $user->setEnabled(false);
            $user->setRoles(array('ROLE_USER'));

            $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
            $userManager->updateUser($user);
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::USER_CREATED, $event);
            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('leden_lijst');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::USER_CREATED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return $this->render('MijnsubclubBundle:user:new.html.twig', array(
            'form' => $form->createview()
        ));
    }

    /**
     * @Route("/admin/leden/{username}/verwijder", name="lid_verwijder")
     * @param Request $request
     * @param         $username
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, $username) {
        $user = $this->get('fos_user.user_manager')->findUserBy(array('username' => $username));
        $this->get('fos_user.user_manager')->deleteUser($user);

        $response = new RedirectResponse($this->generateUrl('leden_lijst'));

        return $response;
    }

    /**
     * User can edit own stuff
     *
     * @Route("/profile/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function userEditAction(Request $request)
    {
        $user = $this->getUser();

        $editForm = $this->createForm('MijnsubclubBundle\Form\Type\UserprofileType', $user)
            ->add('Opslaan', SubmitType::class);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }

        return $this->render('@Mijnsubclub/user/userEdit.html.twig', array(
            'transactie' => $user,
            'edit_form' => $editForm->createView(),
        ));
    }
}
