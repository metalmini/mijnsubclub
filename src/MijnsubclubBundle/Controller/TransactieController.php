<?php

namespace MijnsubclubBundle\Controller;

use MijnsubclubBundle\Entity\Transactie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Transactie controller.
 *
 */
class TransactieController extends Controller
{
    /**
     * Lists all transactie entities.
     *
     * @Route("/admin/transactie", name="admin_transactie_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $transacties = $em->getRepository('MijnsubclubBundle:Transactie')->findAll();

        return $this->render('@Mijnsubclub/transactie/index.html.twig', array(
            'transacties' => $transacties,
        ));
    }

    /**
     * Creates a new transactie entity.
     *
     * @Route("/admin/lid/{user_id}/transactie/new", name="admin_transactie_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $user_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, $user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $transactie = new Transactie();
        $user = $em->getRepository('MijnsubclubBundle:User')->find($user_id);
        $transactie->setUser($user);
        $form = $this->createForm('MijnsubclubBundle\Form\Type\TransactieType', $transactie)
            ->add('Opslaan', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($transactie);
            $em->flush();

            return $this->redirectToRoute('lid_detail', array('id' => $transactie->getUser()->getId()));
        }

        return $this->render('@Mijnsubclub/transactie/new.html.twig', array(
            'transactie' => $transactie,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a transactie entity.
     *
     * @Route("/admin/transactie/{id}", name="admin_transactie_show")
     * @Method("GET")
     */
    public function showAction(Transactie $transactie)
    {
        $deleteForm = $this->createDeleteForm($transactie);

        return $this->render('@Mijnsubclub/transactie/show.html.twig', array(
            'transactie' => $transactie,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing transactie entity.
     *
     * @Route("/admin/transactie/{id}/edit", name="admin_transactie_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Transactie $transactie)
    {
        $deleteForm = $this->createDeleteForm($transactie);
        $editForm = $this->createForm('MijnsubclubBundle\Form\Type\TransactieType', $transactie)
            ->add('Opslaan', SubmitType::class);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_transactie_edit', array('id' => $transactie->getId()));
        }

        return $this->render('@Mijnsubclub/transactie/edit.html.twig', array(
            'transactie' => $transactie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a transactie entity.
     *
     * @Route("/admin/transactie/{id}", name="admin_transactie_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Transactie $transactie)
    {
        $form = $this->createDeleteForm($transactie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($transactie);
            $em->flush();
        }

        return $this->redirectToRoute('leden_lijst');
    }

    /**
     * Creates a form to delete a transactie entity.
     *
     * @param Transactie $transactie The transactie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Transactie $transactie)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_transactie_delete', array('id' => $transactie->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
