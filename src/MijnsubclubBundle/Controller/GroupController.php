<?php

namespace MijnsubclubBundle\Controller;

use FOS\UserBundle\Event\FilterGroupResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GroupEvent;
use FOS\UserBundle\FOSUserEvents;
use MijnsubclubBundle\Form\Type\GroupType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class GroupController extends Controller
{
    /**
     * @Route("/admin/groepen", name="groepen_lijst")
     */
    public function indexAction()
    {
        $groups = $this->get('fos_user.group_manager')->findGroups();

        return $this->render('MijnsubclubBundle:group:index.html.twig', array('groepen' => $groups));
    }

    /**
     * @Route("/admin/groep/{id}/aanpassen", name="groep_aanpassen")
     * @param Request $request
     * @param $id
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        $group = $this->get('fos_user.group_manager')->findGroupBy(array('id' => $id));

        $form = $this->createForm(GroupType::class, $group);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $group = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            return $this->render('MijnsubclubBundle:group:detail.html.twig', array('groep' => $group));
        }

        return $this->render('MijnsubclubBundle:group:edit.html.twig', array(
            'form' => $form->createview(),
            'groep' => $group,
        ));
    }

    /**
     * @Route("/admin/groep/{id}", name="groep_detail", requirements={"id" = "\d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction($id)
    {
        $group = $this->get('fos_user.group_manager')->findGroupBy(array('id' => $id));

        return $this->render('MijnsubclubBundle:group:detail.html.twig', array('groep' => $group));
    }

    /**
     * @Route("/admin/groep/nieuw", name="groep_nieuw")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        /** @var $groupManager \FOS\UserBundle\Model\GroupManagerInterface */
        $groupManager = $this->get('fos_user.group_manager');
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.group.form.factory');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $group = $groupManager->createGroup('');

        $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_INITIALIZE, new GroupEvent($group, $request));

        $form = $formFactory->createForm();
        $form->setData($group);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_SUCCESS, $event);

            $groupManager->updateGroup($group);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('groepen_lijst');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_COMPLETED, new FilterGroupResponseEvent($group, $request, $response));

            return $response;
        }

        return $this->render('MijnsubclubBundle:group:new.html.twig', array(
            'form' => $form->createview()
        ));
    }

    /**
     * Delete one group.
     *
     * @Route("/admin/groep/{naam}/verwijder", name="groep_verwijder")
     * @param Request $request
     * @param         $naam
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, $naam)
    {
        $group = $this->get('fos_user.group_manager')->findGroupBy(array('name' => $naam));
        $this->get('fos_user.group_manager')->deleteGroup($group);

        $response = new RedirectResponse($this->generateUrl('groepen_lijst'));

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(FOSUserEvents::GROUP_DELETE_COMPLETED, new FilterGroupResponseEvent($group, $request, $response));

        return $response;
    }
}
