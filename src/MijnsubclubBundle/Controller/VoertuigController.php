<?php

namespace MijnsubclubBundle\Controller;

use MijnsubclubBundle\Entity\Rdwresponse;
use MijnsubclubBundle\Entity\Voertuig;
use MijnsubclubBundle\Form\Type\VoertuigType;
use MijnsubclubBundle\MijnsubclubBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class VoertuigController extends Controller
{
    /**
     * @Route("/voertuigen", name="voertuigen_lijst")
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('MijnsubclubBundle:Voertuig');
        $subarus = $repository->findBy(array('merk' => 'subaru'));

        return $this->render('MijnsubclubBundle:Voertuig:index.html.twig', array(
            'voertuigen' => $subarus
        ));
    }

    /**
     * @Route("/voertuig/toevoegen", name="voertuig_nieuw")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $voertuig = new Voertuig();
        $voertuig->setUser($this->getUser());

        $form = $this->createForm(VoertuigType::class, $voertuig)
            ->add('Opslaan', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($voertuig);
            $entityManager->flush();

            $this->addFlash('success', 'Voertuig succesvol toegevoegd!');

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render('MijnsubclubBundle:Voertuig:new.html.twig', [
            'voertuig' => $voertuig,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/voertuig/{id}/wijzigen", requirements={"id": "\d+"}, name="voertuig_wijzigen")
     * @param Voertuig $voertuig
     * @param Request  $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Voertuig $voertuig, Request $request)
    {
        if ($this->getUser() !== $voertuig->getUser() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->denyAccessUnlessGranted('ROLE_EDIT', $voertuig, 'You cannot edit this item.');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(VoertuigType::class, $voertuig);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->flush();
            return $this->redirectToRoute('fos_user_profile_show');
        }
        return $this->render('MijnsubclubBundle:Voertuig:edit.html.twig', [
            'voertuig' => $voertuig,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/voertuig/{id}", name="voertuig_detail")
     * @param Voertuig $voertuig
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction(Voertuig $voertuig)
    {
        if ($this->getUser() !== $voertuig->getUser() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->denyAccessUnlessGranted('ROLE_EDIT', $voertuig, 'You cannot edit this item.');
        }

        $kenteken = strtoupper($voertuig->getKenteken());
        $kenteken = str_replace('-', '', $kenteken);

        /** @var \GuzzleHttp\Client $client */
        $client   = $this->get('guzzle.client.api_rdw');
        $response = $client->get('?kenteken=' . $kenteken);

        $content = (string)$response->getBody();

        $serializer = $this->get('serializer');

        $result = $serializer->deserialize($content, 'MijnsubclubBundle\Entity\Rdwresponse[]', 'json');

        return $this->render('MijnsubclubBundle:Voertuig:detail.html.twig', array(
            'voertuig' => $voertuig,
            'rdw_response' => $result
        ));
    }

    /**
     * @Route("/rdw.{_format}",
     *     defaults={"_format": "html"},
     *     options = { "expose" = true },
     *     requirements={
     *         "_format": "html|json",
     *     },
     *     name="rdw_call"
     * )
     * @param Request $request
     * @param         $_format
     * @return Response
     */
    public function rdwcallAction(Request $request, $_format)
    {
        /** @var \GuzzleHttp\Client $client */
        $client   = $this->get('guzzle.client.api_rdw');

        if ($kenteken = $request->query->get('kenteken')) {
            $kenteken = strtoupper($kenteken);
            $kenteken = str_replace('-', '', $kenteken);

            $response = $client->get('?kenteken=' . $kenteken);
        } else {
            $response = $client->get('');
        }

        $content = (string)$response->getBody();

        $serializer = $this->get('jms_serializer');

        if ($_format == 'json'){
            return new Response($content);
        } else {
            return new Response($serializer->deserialize($content, 'MijnsubclubBundle\Entity\Rdwresponse[]', $_format));
        }

    }

}
