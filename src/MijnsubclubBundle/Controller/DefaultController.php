<?php

namespace MijnsubclubBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }

    /**
     * @Route("/SCN", name="scn")
     */
    public function scnAction()
    {
        return $this->render('MijnsubclubBundle:Default:scn.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction()
    {
        return $this->render('MijnsubclubBundle:Default:contact.html.twig');
    }

    /**
     * @Route("/changelog", name="app_changelog")
     */
    public function changelogAction()
    {
        $changelog_path = $this->get('kernel')->getRootDir().'/../CHANGELOG.md';
        $changelog_html = $this->container->get('markdown.parser')->transformMarkdown(file_get_contents($changelog_path));

        return $this->render('default/changelog.html.twig', [
                'changelog' => $changelog_html
            ]
        );
    }
}
