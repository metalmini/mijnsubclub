<?php

namespace MijnsubclubBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VoertuigControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/voertuigen');
    }

    public function testNew()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/voertuig/toevoegen');
    }

    public function testEdit()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/voertuig/{id}/wijzigen');
    }

    public function testDetail()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/voertuig/{id}');
    }

}
