<?php

namespace MijnsubclubBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use MijnsubclubBundle\Entity\Verbruik;

class KilometerdiffListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $verbruik = $args->getObject();
        if (!$verbruik instanceof Verbruik) {
            return;
        }

        $em = $args->getEntityManager();
        $lastVerbruik = $em->getRepository('MijnsubclubBundle:Verbruik')->findHighestTellerstand($verbruik->getVoertuig()->getId());

        $diff = 0;
        if ($lastVerbruik) {
            $diff = $verbruik->getTellerstand() - $lastVerbruik->getTellerstand();
        }

        $verbruik->setTellerstandverschil($diff);
    }
}