<?php

namespace MijnsubclubBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VerbruikType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tellerstand')
            ->add('liter')
            ->add('totaalbedrag')
            ->add(
                'soortbrandstof',
                ChoiceType::class,
                array(
                    'choices' => array(
                        '95' => '95',
                        '98' => '98',
                        '105' => '105',
                        'diesel' => 'diesel',
                        'lpg' => 'lpg',
                        'anders' => 'anders',
                    ),
                )
            )
            ->add(
                'getankedOp',
                DateType::class,
                array(
                    'widget' => 'single_text',
                    'html5' => false,
                    'attr' => ['class' => 'js-datepicker'],
                )
            )
            ->add('bijzonderheden', TextareaType::class, array(
                'required' => false
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'MijnsubclubBundle\Entity\Verbruik',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mijnsubclubbundle_verbruik';
    }


}
