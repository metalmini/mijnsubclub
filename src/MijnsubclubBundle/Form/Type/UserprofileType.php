<?php

namespace MijnsubclubBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserprofileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('voornaam');
        $builder->add('tussenvoegsel');
        $builder->add('achternaam');
        $builder->add('forumnaam');
        $builder->add('straatennummer', TextType::class, array(
                        'label'    => 'Straatnaam en huisnummer')
                    );
        $builder->add('postcode');
        $builder->add('plaats');
        $builder->add('land');
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MijnsubclubBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }


}
