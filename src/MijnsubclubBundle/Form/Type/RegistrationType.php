<?php

namespace MijnsubclubBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('plainPassword');
        $builder->add('voornaam');
        $builder->add('tussenvoegsel');
        $builder->add('achternaam');
        $builder->add('forumnaam');
        $builder->add('straatennummer', TextType::class, array(
                'label'    => 'Straatnaam en huisnummer')
        );
        $builder->add('postcode');
        $builder->add('plaats');
        $builder->add('land');
        $builder->add('groups');
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'app_user_registration';
    }
}
