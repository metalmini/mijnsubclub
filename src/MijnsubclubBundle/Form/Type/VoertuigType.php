<?php

namespace MijnsubclubBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VoertuigType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'naam',
                null,
                array(
                    'required' => true,
                    'attr' => array('class' => 'required')
                )
            )
            ->add(
                'kenteken',
                null,
                array(
                    'required' => true,
                    'attr' => array('class' => 'required')
                )
            )
            ->add(
                'merk',
                null,
                array(
                    'required' => false,
                    'attr' => array('readonly' => 'readonly')
                )
            )
            ->add(
                'model',
                null,
                array(
                    'required' => false,
                    'attr' => array('readonly' => 'readonly')
                )
            )
            ->add(
                'type',
                null,
                array(
                    'required' => false,
                    'attr' => array('readonly' => 'readonly')
                )
            )
            ->add(
                'bouwjaar',
                null,
                array(
                    'required' => false,
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'MijnsubclubBundle\Entity\Voertuig',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mijnsubclubbundle_voertuig';
    }


}
